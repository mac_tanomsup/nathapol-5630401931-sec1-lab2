package tanomsup.nathapol.lab2;
class Greeting {
	public static void main(String[] args) {
		if (args.length > 2) {
	       System.out.println("My favorite teacher's is "+ args[0] );
	       System.out.println("The teacher taught me at "+ args[1] + " which is in " + args[2]);		
		} else {
	           System.out.print("Greetings <teacher name> <school name>");
	     	   System.out.println(" <province name>");  }
	}
}
